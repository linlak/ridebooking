# Contract Kapital Ride Booking

Ride booking is a php library designed to add ridebooking, car pooling, delivery and cargo transportation to our api.

## Installation

To install this library you need have composer installed and ssh access if you are hosting it on a live server. Run the command below in your terminal or cmd window. 

    composer require linlak/ridebooking

## Getting Started

This library has been designed designed as a plug play plugin with all the functionality covered and all that you need to have a ridebooking endpoint to your api. By installing and initializing this library with the required arguments is enough to give you a firm foundation.

## Initialization

To initialize this library you have to create an instance of **RidesBooking\\TripBootstrap** or extend it in your class.

This class houses all the **methods** to 

- Register driver
- Register vehicle
- Confirm driver
- Verify vehicle
- Assign driver
- Find nearby drivers

And many more functions as shown in the examples below. For a quick start check the examples folder.

Let's start by creating a file that will bootstrap our library

**bootrap.php**

```php

    <?php
        require_once(__DIR__.'../../vendor/autoload.php');
        // header('Access-Control-Allow-Origin: *');


        use RidesBooking\TripBootstrap;

        /**
         * @var \RidesBooking\TripBootstrap
         */
        $tripCtonroller = new TripBootstrap('localhost:3306', 'trips', 'root', '');

        return $tripCtonroller;
```

The **TripBootstrap** takes the follow arguments.

- $dbHost in this case *localhost:3306* only mysql database is supported
- $dbName the database you are using to store data
- $dbUser the user with write permissions
- $dbPass the user's password
- $shouldDelete this is a boolen field that should be used during setup only it is aimed at deleting the existing tables everytime the library is bootstraped. 

The last argument defaults to false you should make it false or ommit it to avoid losing data

**Note:**

This only deletes tables with **ridebooking_** prefix not all tables in the database. If you other tables in the database you need to follow the steps in the mysql documentation


## Register Driver

To register a driver simply include the **bootstrap.php** into your php file as shown in the example bellow.

**trips/drivers/register.php**

```php
    <?php

    require_once('../../bootstrap.php');


        $status = 'failed';
        $message = 'Sorry fix a few things';
        $output = [];


        $data = $tripCtonroller->retrievePostData();

        if (isset($data) && isset($data['name']) && isset($data['permit_class'])) {
            $name = $data['name'];
            $permit_class = $data['permit_class'];
            $has_vehicle = (isset($data['has_vehicle'])) ? $data['has_vehicle'] : false;
            $result = $tripCtonroller->registerDriver((int) $_GET['id'], $name, $permit_class, $has_vehicle);
            switch ($result[0]) {
                case 'error':
                    $message = 'Error saving data try again';
                    break;
                case 'exists':
                    $message = 'Driver already registered';
                    $status = 'exists';
                    $output['driver'] = $result[1]['id'];
                    break;
                case 'inserted': 
                    $status = 'success';
                    $message = 'Data saved successfully';
                $driver = $result[1];

                switch ($driver['status']) {
                    case 'pending':
                        $level = 1;
                        $message = 'You need to complete your registration';
                        break;
                    case 'verified':
                        $level = 2;
                        $message = 'All good';
                        break;
                    case 'deleted':
                        $level = 3;
                        $message = 'Your info was deleted';
                }
                if ($driver['status'] != 'deleted') {
                    $formartedDriver = [
                        'id' => (int) $driver['id'],
                        'name' => $driver['name'],
                        'permit_class' => $driver['permit_class'],
                        'has_vehicle' => (bool) $driver['has_vehicle'],
                        'status' => $driver['status']
                    ];
                    if ($driver['image_url']) {
                        $formartedDriver['image_url'] = $driver['image_url'];
                    }
                    $output['driver'] = $formartedDriver;
                }
                $output['level'] = $level;
                break;
                default:
                $message = "Unknown error!";
                    break;
            }        
        } else {
            $message = "Invalid data submitted";
        }

        $output['status'] = $status;
        $output['message'] = $message;

        $tripCtonroller->echoJson($output);
```

Above is an example with all the validation steps to register a driver

Let's break it down

The main function here is the **registerDriver** on the **TripBootstrap** instance in this case **$tripController** and it takes following arguments.

- $user_id - The id of the user being registered as a driver
- $name - The name of the user being registered as driver
- $permit_class - The permit class of the driver.
- $has_vehicle - A boolean field indicating if a user has his own vehicle.
- $admin_id - This is the id of the admin registering a user as a driver. Optional if data is from a user's form trying to register as driver

This function returns the following data

    array($status, $driver)

The 