<?php

namespace  RidesBooking;

use LinWorld\Db\Support\Data\Database;
use RidesBooking\Constants\TripTables;
use RidesBooking\Support\Traits\CreatesTables;
use RidesBooking\Support\Traits\HandlesRequest;
use RidesBooking\Support\Traits\ManagesDrivers;
use RidesBooking\Support\Traits\ManagesTrips;
use RidesBooking\Support\Traits\Validator;

class TripBootstrap extends Database
{

    use HandlesRequest, Validator, CreatesTables, ManagesDrivers, ManagesTrips;

    private $dbName = "";

    private $found_tables = [];
    public function __construct($host, $dbName, $dbUser, $dbPass, $shouldDrop = false)
    {
        parent::__construct($host, $dbName, $dbUser, $dbPass);
        $this->dbName = $dbName;
        if ($shouldDrop) {

            $this->dropExits();
        }
        $this->createTables();
    }

    final public function bookTrip(
        $user_id,
        $start_lat,
        $dest_lat,
        $start_lng,
        $dest_lng,
        $start_name,
        $dest_name,
        $pay_type,
        // search radius
        $distance = 5
    ) {

        return false;
        $driver = $this->findFirstDriver($start_lat, $start_lng, $distance);


        if ($driver) {
            $sql = "INSERT INTO `" . TripTables::TRIP . "` 
            (`user_id`, `start_position`, `dest_position`, `start_name`, `dest_name`, `pay_type`)
             VALUES 
             (:user_id, ST_GeomFromText('POINT({$start_lat} {$start_lng})', 4326), ST_GeomFromText('POINT({$dest_lat} {$dest_lng})', 4326), :start_name, :dest_name, :pay_type);";
            $this->query($sql);
            $this->bind(':user_id', $user_id);
            $this->bind(':start_name', $start_name);
            $this->bind(':dest_name', $dest_name);
            $this->bind(':pay_type', $pay_type);
            $this->debugDumpParams();
        }
        exit();
        return $sql;
    }

    final public function getNearByDrivers(string $tripType, string $vehicleType, float $lat, float $lng)
    {
        //
    }

    final public function trackBooking($distance, $user_id)
    {
        # code...
    }
    /**
     * @param float {$lat}
     * @param float {$lng}
     * @param float {$distance}
     * 
     * @return array
     */
    final public function bookRide(float $lat = 0.09, float $lng = 0.18, float $distance = 30)
    {
        $output = [];
        $sql = "SELECT `id`, `name`, 
             ST_Latitude(`position`) as `lat`, 
             ST_Longitude(`position`) as `lng`, 
             ST_Distance_Sphere( `position`, ST_GeomFromText( 'POINT({$lat} {$lng})', 4326, 'axis-order=lat-long' ) )  AS `distance` 
             FROM `locations_earth` WHERE ST_Distance_Sphere( `position`, ST_GeomFromText( 'POINT({$lat} {$lng})', 4326, 'axis-order=lat-long' ) ) <= ({$distance}*1000)+10
             ORDER BY 
             distance ASC 
              LIMIT 15";
        $this->query($sql);
        $output = $this->resultset($sql);
        return $output;
    }


    final public function assignDriver($vehicle_id, $driver_id, $admin_id = null)
    {
        # code...
    }

    // ST_DWithin 






    final public function addVehical()
    {
    }

    final public function comissionDriver($user_id, $lat, $lng)
    {
        // 
    }


    final public function confirmVehicle()
    {
        # code...
    }


    final public function uncomissionDriver($user_id)
    {
        # code...
    }

    final public function removeVehicle(int $vehicle_id)
    {
        # code...
    }
}
