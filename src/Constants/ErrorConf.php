<?php
namespace RidesBooking\Constants;
class ErrorConf{
	const U_ALLOWED='Remove unwanted Characters';
	const U_REQUIED='Field can not be blank';
	const U_NAME='Enter a valid username, Phone or Email';
	const Fix_THINGS='Sorry. You need to fix a few things...';
}