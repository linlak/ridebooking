<?php

namespace RidesBooking\Constants;


class TripTables {



    private function __construct()
    {
        # code...
    }

    const BOOKING_PARTERN = "ridebooking_";
    const DRIVER = "ridebooking_drivers";
    const DRIVER_RATING = "ridebooking_driver_rating";
    const VEHICLES = "ridebooking_vehicles";
    const VEHICLES_DRIVER = "ridebooking_vehicledrivers";
    const TRIP = "ridebooking_trips";
    const TRIP_ROUTES = "ridebooking_trip_routes";
    const TRIP_DEST = "ridebooking_trip_dest";
    const COMISSIONED = "ridebooking_drivercomissioned";
    const VEHICLE_IMAGES = "ridebooking_vehicleimages";
    const DRIVER_PERMITS = "ridebooking_permits";
    const VEHICLE_DOCS = "ridebooking_docs";
    const RATING = "ridebooking_driverrating";
    const VEHICLE_TYPES = "ridebooking_vehicletype";
    



}