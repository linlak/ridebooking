<?php

namespace RidesBooking\Support\Traits;

use RidesBooking\Constants\TripTables;

trait CreatesTables {
    private function dropExits()
    {
        $this->loadExists();
        $sql = 'DROP TABLE ' .
            join(',', $this->found_tables) .
            ';';


        if ($sql !== "DROP TABLE ;") {
            $sql = "SET FOREIGN_KEY_CHECKS = 0; " . $sql . " SET FOREIGN_KEY_CHECKS = 1; ";
            $this->newtable($sql);
            $this->loadExists();
        }
    }
    
    private function loadExists()
    {
        $this->found_tables = [];
        $sql1 = "SELECT TABLE_NAME as table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=:table_schema AND TABLE_NAME LIKE :table_partern";
        $this->query($sql1);
        $this->bind(':table_schema', $this->dbName);
        $this->bind(':table_partern', TripTables::BOOKING_PARTERN . '%');
        if ($fd = $this->resultSet()) {
            for ($i = 0; $i < count($fd); $i++) {
                array_push($this->found_tables, $fd[$i]['table_name']);
            }
        }
    }

    private function createTables()
    {
        $this->loadExists();
        $sql = "";

        if (!in_array(TripTables::TRIP_ROUTES, $this->found_tables)) {
            // SRID 4326
            //INSERT INTO `locations_earth`(`name`, `position`) VALUES
            //  ('point_1', ST_GeomFromText('POINT(0.09000 0.18000)', 4326)),
            $sql .= " 
            CREATE TABLE  `" . TripTables::DRIVER . "` (
                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                      `name` varchar(100) NOT NULL,
					  `user_id` int(99) NOT NULL,
					  `status` enum('pending','verified','deleted') NOT NULL DEFAULT 'pending',
					  `has_vehicle`  enum('true','false') NOT NULL DEFAULT 'false',
					  `created_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `verified_at`  datetime NULL,
                      `admin_id` int(99) NULL,
                      `permit_class` varchar(10) NOT NULL,
                      `permit_image` TEXT  NULL,
                      `image_url` TEXT  NULL,
                      `driver_no` int(11) NOT NULL,
                      `updated_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `is_active` enum('true', 'false') NOT NULL DEFAULT 'true',
                      `reason` TEXT NULL,
                      PRIMARY KEY (`id`),
                      UNIQUE (`user_id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=armscii8;
            
             CREATE TABLE  `" . TripTables::VEHICLES . "` (
					  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                      `model` varchar(256) NOT NULL,
                      `name_plate` varchar(10) NOT NULL,
                      `seats` int(11)  NULL,
                      `max_weight` int(11) NOT NULL,
                      `image_cover` TEXT NULL,
                      `coverage` float(16, 4) NULL,
                      `weight_unit` enum('kg', 'tones'), 
                      `owner_id` int(11) NOT NULL,
                      `charge_types` set('perseat', 'pertone', 'perkm', 'perhour', 'permin'),
                      `owner_type` varchar(256) NOT NULL,
                      `category` enum('bicycle', 'bus', 'boda','tricycle', 'cart', 'truck', 'car') NULL,
					  `trip_types` set('ride','delivery', 'order', 'cargo') NOT NULL,
					  `created_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id`),
                      UNIQUE (`name_plate`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=armscii8;
             
             CREATE TABLE  `" . TripTables::VEHICLES_DRIVER . "` (
					  `driver_id` bigint(20) NOT NULL,
                      `vehicle_id` bigint(20) NOT NULL,
                      `admin_id` bigint(20)  NULL,                      
					  `is_active`  enum('true', 'false') NOT NULL DEFAULT 'false',
					  `verified_at`  datetime NULL,
					  `created_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                      PRIMARY KEY (`driver_id`),
                      UNIQUE (`vehicle_id`),                 
                      CONSTRAINT `fk_vehicle_driver`  FOREIGN KEY (driver_id) REFERENCES " . TripTables::DRIVER . "(id) ON DELETE CASCADE ON UPDATE CASCADE,                 
                      CONSTRAINT `fk_driver_vehicle`  FOREIGN KEY (vehicle_id) REFERENCES " . TripTables::VEHICLES . "(id) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=armscii8; 
                        
                CREATE TABLE  `" . TripTables::VEHICLE_IMAGES . "` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `vehicle_id` bigint(20) NOT NULL,
                    `image_uri` TEXT NOT  NULL,
                    `created_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `updated_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     PRIMARY KEY (`id`),
                    CONSTRAINT `fk_vehicle_image`  FOREIGN KEY (vehicle_id) REFERENCES " . TripTables::VEHICLES . "(id) ON DELETE CASCADE ON UPDATE CASCADE
                    ) ENGINE=InnoDB DEFAULT CHARSET=armscii8;
        
                CREATE TABLE  `" . TripTables::COMISSIONED . "` (
					  `driver_id` bigint(20) NOT NULL,
                      `position` point NOT NULL SRID 4326,
					  `on_trip` enum('true','false') NOT NULL DEFAULT 'false',
					  `created_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                      PRIMARY KEY (`driver_id`),
                      SPATIAL INDEX(`position`),                      
                      CONSTRAINT `fk_comissioned_driver`  FOREIGN KEY (driver_id) REFERENCES " . TripTables::DRIVER . "(id) ON DELETE CASCADE ON UPDATE CASCADE
                    ) ENGINE=InnoDB DEFAULT CHARSET=armscii8;

                 
                    CREATE TABLE  `" . TripTables::TRIP . "`(
                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
					  `vehicle_id` bigint(20) NULL,
                      `trip_type` enum('ride','delivery', 'order', 'cargo') NOT NULL,
                      `amount` float(16, 4) NULL,
                      `cargo_unit` enum('kg', 'tones', 'ltrs', 'seats', 'other') NULL,
                      `other_unit` varchar(10) NULL,
                      `cargo_type` enum('goods', 'food', 'produce', 'materials', 'cash'),
                      `shop_id` int(11) NULL,
					  `driver_id` bigint(20)  NULL,
                      `user_id` int(99) NOT NULL,

                      `start_position` point NOT NULL SRID 4326,
                      `dest_position` point NOT NULL SRID 4326,
                      `start_name` TEXT NULL,
                      `dest_name` TEXT NULL,
                      `estmated_pay`  BIGINT(20) NOT NULL,

                      `pay_type`  ENUM('cash', 'cashless', 'bonus') NOT NULL,
                      `cancel_reason` TEXT NULL,
                      `started` ENUM('true', 'false') NOT NULL DEFAULT 'false',
					  `started_at` datetime  NULL,
					  `canceled_at` datetime  NULL,
					  `assigned_at` datetime  NULL,
                      `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,                      
                      `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id`),
                      SPATIAL INDEX(`start_position`),
                      SPATIAL INDEX(`dest_position`),               
                      CONSTRAINT `fk_trip_vehicle`  FOREIGN KEY (vehicle_id) REFERENCES " . TripTables::VEHICLES . "(id) ON DELETE CASCADE ON UPDATE CASCADE               
                ) ENGINE=InnoDB DEFAULT CHARSET=armscii8;
        
                CREATE TABLE  `" . TripTables::TRIP_ROUTES . "`(
                    `id` bigint(20) NOT NULL  AUTO_INCREMENT,
                    `trip_id` bigint(20) NOT NULL,
                    `route_position` point NOT NULL SRID 4326,
					`caputured_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    SPATIAL INDEX(`route_position`), 
                    PRIMARY KEY (`id`),
                    CONSTRAINT `fk_trip_route`  FOREIGN KEY (trip_id) REFERENCES " . TripTables::TRIP . "(id) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=armscii8;

                CREATE TABLE  `" . TripTables::TRIP_DEST . "`(
                    `trip_id` bigint(20) NOT NULL,
                    `stop_position` point NOT NULL SRID 4326,
                    `amount_paid` BIGINT(20) NOT NULL DEFAULT '0',
                    `paytype`  ENUM('cash', 'cashless', 'bonus') NOT NULL,
                    `place_name` TEXT NULL,
                    `stopped_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`trip_id`),
                    SPATIAL INDEX(`stop_position`), 
                    CONSTRAINT `fk_trip_dest`  FOREIGN KEY (trip_id) REFERENCES " . TripTables::TRIP . "(id) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=armscii8;
                ";            
        }

        
        $sql = (string) trim($sql);
        if ($sql !== "") {
            $this->newtable($sql);
            $this->loadExists();
        }
    }
}