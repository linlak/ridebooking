<?php
namespace RidesBooking\Support\Traits;

use RidesBooking\Constants\ErrorConf;

trait Validator {
	
	use Func;

	private $_errs=array();
	protected $input_type;
	public function __construct(){
	}
	public function setErr($ErrKey,$ErrMsg){
		if(!array_key_exists($ErrKey,$this->_errs)){
			$this->_errs[$ErrKey]=$ErrMsg;
		}
	}
	public function getErrs($ErrKey=''){
		if(empty($this->_errs)||($ErrKey!=''&&!array_key_exists($ErrKey,$this->_errs))){return null;}
		if($ErrKey!=''){
			return $this->_errs[$ErrKey];
		}else
		{
			return $this->_errs;
		}
	}
	public function alphametric($value){
	 if (preg_match("/^[a-zA-Z0-9_\,\'\"\.\?\/\!\-\@\:\(\)\&\;\%\\r\\n\\t ]*$/",$value)) {
		return true;
		}
		return false;
	}
	public function alphanumeric($value){
	 if (preg_match("/^[a-zA-Z0-9_ ]*$/",$value)) {
		  return true;
		  }
		  return false;
	}
	public function numeric($value){
	 if (preg_match("/^[0-9]*$/",$value)) {
			return true;
		}
		return false;
	}
	// validate only letters and white space
	public function all_letters($value){
		 if (preg_match("/^[a-zA-Z ]*$/",$value)) {
	  return true;
	}
	return false;
	}
	public function valphone($string) {
	$string=filter_var($string,FILTER_SANITIZE_NUMBER_INT);
	$string=$this->rep_text('-','',$string);
    if ( preg_match( '/^[+]?([\d]{0,3})?[\(\.\-\s]?([\d]{3})[\)\.\-\s]*([\d]{3})[\.\-\s]?([\d]{4})$/', $string ) ) {
        return TRUE;
    } else {
        return FALSE;
    }
	}
	public function  hasminlength($value1, $value2){
	if(strlen($value2)>=$value1){
		return true;
	}
	return false;
	}
	public function hasmaxlength($value1, $value2){
	if(strlen($value2)<=$value1){
		return true;
	}
	return false;
	}
	public function valfixedleng($value1, $value2){
	if(strlen($value2)===$value1){
		return $value2;
	}
	}
	public function hasrange($value1,$value2,$value3){
	if(strlen($value3)>=$value1 && strlen($value3)<=$value2){
		return true;
	}
	return false;
	}
	public function val_url($website){
	$website = filter_var($website, FILTER_SANITIZE_URL);
	//$website = filter_var($website, FILTER_VALIDATE_URL, FILTER_FLAG);
	$website = filter_var($website, FILTER_VALIDATE_URL);
	if($website){
		return true;
	}else{
		return false;
	}
}
	public function haspresence($value){
	$value=$this->str_clear($value);
	if(!empty($value)){
		return true;
	}
	return false;
}
	
	public function chkdselect($value1,$value2){	
	if($value1===$value2){
		return 'selected="selected"';
	}
	
}
	public function val_img_to_dbb($param,$value,$isReq=false){
		if (!empty($_FILES[$param]["name"])) {
			$myFile = $_FILES[$param];
			if ($myFile["error"] === UPLOAD_ERR_OK){
				$info=$myFile['type'];
				$allowed=array('image/jpeg','image/jpg', 'image/png', 'image/gif');
				if(in_array($info, $allowed)){
					return $myFile;
				}else{
					$this->setErr($value,'Only jpeg, png and gif are allowed');
				}
			}else{
				$this->setErr($value,'An error occurred during upload!!');
			}
		}else{
			if ($isReq) {
				$this->setErr($value,'File is required');
			}
		}
		return false;
	}
	public function val_pic($param,$value,$upload_dir,$isReq=false){
		$name="";
if (!empty($_FILES[$param]["name"])) {
    $myFile = $_FILES[$param];
	if ($myFile["error"] === UPLOAD_ERR_OK){
	//limit upload size
	$info=$myFile['type'];
	$allowed=array('image/jpeg','image/jpg', 'image/png', 'image/gif');
	if(in_array($info, $allowed)){
    // ensure a safe filename
    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);	
		// don't overwrite an existing file
		//if directory not present will be created by imageManuplator
    $i = 0;
    $parts = pathinfo($name);
	if (is_dir($upload_dir)){
    while (file_exists($upload_dir.$name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
	}
	return array($myFile,$name);
	}else{
		$this->setErr($value,'Only jpeg, png and gif are allowed');
	}
    }else{
	$this->setErr($value,'An error occurred during upload!!');
	}
	}else{
		if($isReq){
		$this->setErr($value,'Photo is required!!');
		}
	}
	return null;
	}
	public function val_email($email){
	$safe_email = filter_var($email, FILTER_SANITIZE_EMAIL);
	$safe_email = filter_var($email, FILTER_VALIDATE_EMAIL);
	if($safe_email){
		return true;
	}else{
		return false;
	}
	
}
	public function isValid($param,$value,$what,$isReq=false){
		if(!$this->haspresence($param)){if($isReq){$this->setErr($value,ErrorConf::U_REQUIED);} return false;}
		switch($what){
			default:
			throw new \Exception('You must the correct type '.$what.' is not known!..');
			break;
			case 'text':
			if(!$this->all_letters($param)){$this->setErr($value,ErrorConf::U_ALLOWED);return false;}
			break;
			case 'textAlpha':
			if(!$this->alphanumeric($param)){$this->setErr($value,ErrorConf::U_ALLOWED);return false;}
			break;
			case 'alphametric':
			if(!$this->alphametric($param)){$this->setErr($value,ErrorConf::U_ALLOWED);return false;}
			break;
			case 'numeric':
			if(!$this->numeric($param)){$this->setErr($value,'Only numeric Characters are Allowed');return false;}
			break;
			case 'email':
			if(!$this->val_email($param)){$this->setErr($value,'Invalid Email submited');return false;}
			break;
			case 'url':
			if(!$this->val_url($param)){$this->setErr($value,'Invalid Url submited');return false;}
			break;
			case 'phone':
			case 'tel':
			switch($what){
				default:
				$mywhat='Phone Number';
				break;
				case 'tel':
				$mywhat='Telephone';
				break;
			}
			if(!$this->valphone($param)){$this->setErr($value,'Invalid '.$mywhat);return false;}
			break;
			case 'auth_user':
			if(!$this->valphone($param) && !$this->val_email($param) &&!$this->alphametric($param)){
				$this->setErr($value,'Enter a valid username, Phone or Email');return false;
			}
			break;
			case 'fax':
				return true;
				break;
			case 'date':
			return true;
			break;
			case 'auth_pass':
			if(!$this->hasminlength(6,$param)){
				$this->setErr($value,'Password is too Short');return false;
			}
			break;
			
		}
		return true;
	}
	
}