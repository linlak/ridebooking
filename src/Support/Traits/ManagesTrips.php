<?php

namespace RidesBooking\Support\Traits;

use RidesBooking\Constants\TripTables;

trait ManagesTrips {
    private function getTrip($trip_id, $user_id, $tripBy = "user")
    {
        $sql = "
            SELECT 
            a.id, a.start_name, a.dest_name,
            a.estmated_pay, a.pay_type,
            a.cancel_reason, a.canceled_at,
            a.started, a.started_at,
            a.assigned_at,
            ST_Latitude(a.start_position) as `start_lat`, 
            ST_Longitude(a.start_position) as `start_lng`,
            ST_Latitude(a.dest_position) as `dest_lat`, 
            ST_Longitude(a.dest_position) as `dest_lng`,
            ST_Distance_Sphere( a.start_position, a.dest_position)  AS `estimated_distance`,
            ST_Distance_Sphere( a.start_position, d.stop_position)  AS `estimated_stop_distance`,
            a.created_at, a.updated_at,

            b.name, b.id as driver_id,
            c.name_plate, c.model, c.id as vehicle_id, c.category, c.charge_types,

            d.stopped_at,

            ST_Latitude(d.stop_position) as `stop_lat`, 
            ST_Longitude(d.stop_position) as `stop_lng`
            ";
        $sql .= "
         FROM `" . TripTables::TRIP . "` a 
        ";
        $sql .= " LEFT JOIN `" . TripTables::DRIVER . "` b ON a.driver_id = b.id ";
        $sql .= " LEFT JOIN `" . TripTables::VEHICLES . "` c ON a.vehicle_id = c.id ";
        $sql .= " LEFT JOIN `" . TripTables::TRIP_DEST . "` d ON a.id = d.trip_id ";
        $sql .= " ";
        $sql .= " WHERE (a.id = {$trip_id}";
        if ($tripBy === 'user') {
            $sql .= " AND a.user_id = {$user_id} ";
        } else if ($tripBy === 'driver') {
            $sql .= " AND b.user_id = {$user_id} ";
        }
        $sql .= ") ";
        $sql .= " LIMIT 1;";
        return $this->retdata($sql);
    }
    final public function startTrip($trip_id, $user_id, float $lat, float $lng)
    {
        $this->beginTransaction();
        $trip = $this->getTrip($trip_id, $user_id, "driver");
        if ($trip) {

            if ($trip['started_at'] === null && $trip['canceled_at'] === null) {
                $this->genUpdate(TripTables::TRIP, [
                    'started' => true,
                    'started_at' => $this->mytime()
                ], [
                    'id' => $trip['id']
                ]);
                $this->addRoutePoint($trip['id'], $lat, $lng);
            }
            $trip = $this->getTrip($trip['id'], $user_id, 'driver');
        }
        $this->endTransaction();
        return $trip;
    }

    final public function endTrip($trip_id, $driver_id, float $lat, float $lng, $place = null)
    {
        $this->beginTransaction();
        $trip = $this->getTrip($trip_id, $driver_id, 'driver');
        if ($trip) {
            if ($trip['stopped_at'] === null){
                $this->addStopPoint($trip, $lat, $lng, $place);

                $trip = $this->getTrip($trip['id'], $driver_id, 'driver');
            }
        }
        $this->endTransaction();
        return $trip;
    }
    final public function userCancelTrip($trip_id, $user_id, string $cancelBy = "user", string $reason = "")
    {
        $this->beginTransaction();
        $trip = $this->getTrip($trip_id, $user_id, $cancelBy);
        if ($trip) {
            if ($trip['started'] === 'false' && $trip['canceled_at'] === \null) {
                $tripToDb = [
                    'canceled_at' => $this->mytime(),
                    'cancel_reason' => $reason
                ];
                $this->genUpdate(TripTables::TRIP, $tripToDb, ['id' => $trip['id']]);
                $this->endTransaction();
                $trip = $this->getTrip($trip['id'], $user_id, $cancelBy);
            }
        }
    }
    public function addRoutePoint($trip_id, $lat, $lng)
    {
        $sql = "INSERT INTO `" . TripTables::TRIP_ROUTES . "` (`route_position`, `trip_id`) VALUES (ST_GeomFromText('POINT({$lat} {$lng})', 4326), {$trip_id})";
        $this->putdata($sql);
    }
    private function addStopPoint(array $trip, $lat, $lng, $place)
    {
        $this->addRoutePoint($trip['id'], $lat, $lng);
        $this->enOnTrip($trip['driver_id'], false, $lat, $lng);
        $sql = "INSERT INTO `" . TripTables::TRIP_DEST . "` 
        (`trip_id`, `stop_position`, `amount_paid`, `paytype`, `place_name`)
         VALUES 
         ({$trip['id']}, ST_GeomFromText('POINT({$lat} {$lng})', 4326), 0, '{$trip['pay_type']}', '$place')";
        $this->putdata($sql);
    }
}