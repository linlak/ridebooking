<?php
namespace RidesBooking\Support\Traits;
use \DateTime as DateTime;
use \DateInterval as DateInterval;
use \DateTimeZone as DateTimeZone;
 trait  Func{
private $_hash_formart="$2y$10$";
private $cost_len=15;
public function __construct(){	
}
final public function base64url_encode($string,$is_64=false){
	if (!$is_64) {
		$string=base64_encode($string);
	}
	return rtrim(strtr($string, '+/', '-_'),'=');
}
function getBaseUrl()
{
    if (PHP_SAPI == 'cli') {
        $trace=debug_backtrace();
        $relativePath = substr(dirname($trace[0]['file']), strlen(dirname(dirname(__FILE__))));
        echo "Warning: This sample may require a server to handle return URL. Cannot execute in command line. Defaulting URL to http://localhost$relativePath \n";
        return "http://localhost" . $relativePath;
    }
    $protocol = 'http';
    if ($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')) {
        $protocol .= 's';
    }
    $host = $_SERVER['HTTP_HOST'];
    $request = $_SERVER['PHP_SELF'];
    return dirname($protocol . '://' . $host . $request);
}
final public function base64url_decode($string,$no_dec=false){
	$string=strtr($string, '-_', '+/').str_repeat('=', 3-(3+strlen($string))%4);
	if (!$no_dec) {
		$string=base64_decode($string);
	}
	return $string;
}
final public function checkHtml($string){
	$start=strpos($string, '<');
	$end=strrpos($string, '>',$start);
	$len=strlen($string);
	if ($end!==false) {
		$string=substr($string, $start);
	}else{
		$string=substr($string, $start,$len-$start);
	}
	libxml_use_internal_errors(true);
	libxml_clear_errors();
	$xml=simplexml_load_string($string);
	return count(libxml_get_errors())==0;
	// return $xml;
}
final public function encHtml($string,$econdeme=0){
	$output='';
	if ($string!=='') {
		if (1===$econdeme) {
			$output=preg_replace('#<#', '{~', $string);
			$output=preg_replace('#>#', '~}', $output);
			$output=preg_replace('#"#', '{{q}}', $output);
		}elseif (0===$econdeme) {
			$output=preg_replace('#{~#', '<', $string);
			$output=preg_replace('#~}#', '>', $output);
			$output=preg_replace('#{{q}}#', '"', $output);
		}
	}
	return $output;
}

final public function isComputer(){
	if(strtolower($this->detectDevice())==='computer'){
		return true;
		}
	}
final public function isMobile(){
	if(strtolower($this->detectDevice())==='mobile'||strtolower($this->detectDevice())==='tablet'){
		return true;
		}
	}
public function imStr($type='png',$data=''){
	if (''!==$data) {
		// return $data;
		return 'data:image/'.$type.';base64,'.$data;
	}
	return '';
}	
final public function my_domain(){
	$pageURL = 'http';
	if (isset($_SERVER["HTTPS"])) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
 $pageURL .= "://";
  $pageURL .= $_SERVER["SERVER_NAME"];
 
 return $pageURL;
}
final public function getReffererDetails(){
	$ref=$_SERVER['HTTP_REFERER'];
	$ref_details=parse_url($ref);
	return $ref_details;
}

final public function curPageURL(){
 $pageURL=$this->my_domain();
  $pageURL .=$_SERVER["REQUEST_URI"];
 return $pageURL;
}
// extract numbers
final public function multiexplode ($delimiters,$string) {   
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  $launch;
}

// clean user input
final public function str_clear($value){
	$bad = array("content-type","bcc:","to:","cc:","href","fuck","basterd","&&","||","??");
 
      return str_ireplace($bad,"",$value);
}
final public function test_input($data){
	$data=$this->str_clear($data);
	$data=trim($data);
	$data=stripslashes($data);
	$data=strip_tags($data);
	$data=htmlspecialchars($data);
	// $data=$this->cleanString($data);
	return $data;
}

final public function trim_all( $str , $what = NULL , $with = ' ' )
    {
        if( $what === NULL )
        {
            //  Character      Decimal      Use
            //  "\0"            0           Null Character
            //  "\t"            9           Tab
            //  "\n"           10           New line
            //  "\x0B"         11           Vertical Tab
            //  "\r"           13           New Line in Mac
            //  " "            32           Space
           
            $what   = "\\x00-\\x20";    //all white-spaces and control chars
        }
       
        return trim(preg_replace("/([".$what."]+)/" , $with , $str ));
    }
 

// replace a certain character form string
final public function rep_text($value1,$value2,$value3){
	 $output=str_replace($value1,$value2,$value3);
	 return $output;
 }
//Function to check if the request is an AJAX request
final public function is_ajax() {
	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}
// Function returns the query string (q or query parameters) from the referrer
final public function get_search_query(){
    $ref_keywords = '';
  
    // Get the referrer to the page
    $referrer = $_SERVER['HTTP_REFERER'];
    if (!empty($referrer))
    {
        //Parse the referrer URL
        $parts_url = parse_url($referrer);
 
        // Check if a query string exists
        $query = isset($parts_url['query']) ? $parts_url['query'] : '';
        if($query)
        {
            // Convert the query string into array
            parse_str($query, $parts_query);
            // Check if the parameters 'q' or 'query' exists, and if exists that is our search query terms.
            $ref_keywords = isset($parts_query['q']) ? $parts_query['q'] : (isset($parts_query['query']) ? $parts_query['query'] : '' );
        }
    }
    return $ref_keywords;
}
final public function getAgent(){
	// $myheaders=getallheaders();
	// return $myheaders['User-Agent'];
	return '';
}
public function is_app(){
	$app="Ugsalons_http_client";
	return preg_match("/".$app."/i", $this->getAgent());
}
final public function detectDevice(){
	$deviceName='others';
	
	$userAgent = $this->getAgent();
	$devicesTypes = array(
        "computer" => array("msie 10", "msie 9", "msie 8", "windows.*firefox", "windows.*chrome", "x11.*chrome", "x11.*firefox", "macintosh.*chrome", "macintosh.*firefox", "opera"),
        "tablet"   => array("tablet", "android", "ipad","Ugsalons_http_client", "tablet.*firefox"),
        "mobile"   => array("mobile ", "android.*mobile", "iphone", "ipod", "Ugsalons_http_client", "opera mobi", "opera mini"),
        "bot"      => array("googlebot", "mediapartners-google", "adsbot-google", "duckduckbot", "msnbot", "bingbot", "ask", "facebook", "yahoo", "addthis")
    );
    if ($userAgent) {
    	# code...
    	foreach($devicesTypes as $deviceType => $devices) {           
        foreach($devices as $device) {
            if(preg_match("/" . $device . "/i", $userAgent)) {
                $deviceName = $deviceType;
            }
        }
    }
    } 	
    return ucfirst($deviceName);
 	}
final public function get_client_ip_env() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
 
    return $ipaddress;
}

final public function my_key(){
	return md5(uniqid(rand()));
}

function clean_str($value){
	$value=$this->str_clear($value);
	//$value=htmlspecialchars($value);
	$value=html_entity_decode($value);
	
	return $value;
}

public function calday($value){
	$then = strtotime($value);
	 $today = time();
	 $difference = $today-$then;
	 $days=floor($difference/(60*60*24));
	return $days;
}
//agecal
public function agecal($dob){
	 if(!empty($dob)){
	 $birthdate = new DateTime($dob);
	 $birthdate->setTimezone($this->myclient_zone());
	 $today = new DateTime('today');
	 $today->setTimezone($this->myclient_zone());
	 $age = $birthdate -> diff($today) -> y;
	 return $age;
	 }else{
		 return false;
	 }
	 
 } 
 
 // add time
 final public function addCust($value,$value1=""){
	 return date("Y-m-d h:i:sa", strtotime($value, strtotime($value1==""?$this->mytime():$value1)));
 }
final public function mytime($format=""){
	$date=new DateTime(date("Y-m-d H:i:s"),$this->myserver_zone());
	return $format==""?$date->format('Y-m-d H:i:s'):$date->format($format);
}
final public function addTime($value,$value1=NULL){	 
	 if(is_null($value1)){
		$date = new DateTime($this->mytime()); 
	 }else{
		$date = new DateTime($value1);
	 }	 
$date->add(new DateInterval('P'.$value.'M'));
return $date->format('Y-m-d H:i:s');
 }
function mychars($value1, $value2){
	 if (preg_match("/$value1/",$value2)) {
  return $value2;
	 }
 }
//random alphametric key
final public function randomstring($len){
$string = "";
$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
for($i=0;$i<$len;$i++)
$string.=substr($chars,rand(0,strlen($chars)),1);
return $string;
}
final public function numstring($len){
$string = "";
$chars = "0123456789";
for($i=0;$i<$len;$i++)
$string.=substr($chars,rand(0,strlen($chars)),1);
return $string;
}
// encrypt password using blowfish	
final public function password_encrypt($password){	
	$salt_length=22;/*'salt'=>$this->generate_salt($salt_length),*/
	$options=['cost'=>$this->cost_len];
	$hash=password_hash($password,PASSWORD_BCRYPT,$options);
	return $hash;
}
final public function check_rehash($hash){
	return password_needs_rehash($hash,PASSWORD_BCRYPT,['cost'=>$this->cost_len]);
}
final private function generate_salt($length){
	$unique_random_string=md5(uniqid(mt_rand(), true));
	$base64_string=base64_encode($unique_random_string);
	$modified_base64_string=str_replace('+','.',$base64_string);
	$salt=substr($modified_base64_string,0,$length);
	return $salt;
}
final public function password_check($password, $existing_hash){
	return password_verify($password,$existing_hash);
}
public function Addfiles($value){
	require_once $value;
}

final public function chkLoc(){
	return isset($_SESSION['Loc']['zone'])&&isset($_SESSION['Loc']['country']);
}
 
 // client timezone
final private function myclient_zone(){	
	 if($this->chkLoc()){
		date_default_timezone_set($_SESSION['Loc']['zone']);		
	 }else{
		 date_default_timezone_set('Africa/Kampala');
	 }
		return new DateTimeZone(date_default_timezone_get());
 }
 // server timezone
final private function myserver_zone(){
    $server_time=ini_get('date.timezone');	
		return new DateTimeZone($server_time);
 }
final public function datecal($value, $formatdate,$isFromClient=""){
	
	 if($value!=""){
	if($isFromClient){
	 $gmtTimezone = $this->myclient_zone();
	 $newdate=new DateTime($value,$gmtTimezone);
	 $newdate->setTimezone($this->myserver_zone());
	}else{	
	 $gmtTimezone =$this->myserver_zone();
	 $newdate=new DateTime($value,$gmtTimezone);
	 $newdate->setTimezone($this->myclient_zone());
	}	 
	 if($formatdate!=""){
	 $mytime=$newdate->format($formatdate);
	 }
	 return $mytime;
	 }
	 return "";
 }

final public function msg($type,$msg,$title=false){
	$res=['type'=>$type,'msg'=>$msg];
	if (false!==$title) {
		$res['title']=$title;
	}
	return $res;
}

// echo json string
public final function echoJson($jsonArray=array()){
	if (!is_array($jsonArray)) {
		# code...
		$jsonArray=array('data'=>$jsonArray);
	}
	header('Content-type: application/json');
	echo(json_encode($jsonArray,JSON_UNESCAPED_SLASHES));
}
//server sent events
final public function streamHeaders(){
	set_time_limit(0);
	ob_get_clean();		
	ignore_user_abort(true);
	header('Content-Type:text/event-stream');
	header('Cache-Control: no-cache');
}
final public function sendMsg($id,$msg,$event="")
{
	
	if (!is_array($msg)) {
		$msg=array('msg'=>$msg);
	}

	if($event!==""){
		echo "event: $event\n";
	}
	echo "id:$id\n";
	echo"data:".json_encode($msg).PHP_EOL.PHP_EOL;	
	echo PHP_EOL;
	$this->flushBuffers();
}
final public function flushBuffers(){
	ob_end_flush();
	ob_flush();
	flush();
	ob_start();
}
final public function  streamRetry(){
	echo 'retry: 1000\n';
	$this->flushBuffers();
}
final public function checkAbort(){
	if (connection_aborted()) {
		header("Content-Type: text/plain");
		echo("hi bye");
		exit();
	}
	return true;
}

}