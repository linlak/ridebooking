<?php

namespace RidesBooking\Support\Traits;

use RidesBooking\Constants\TripTables;

trait ManagesDrivers {
    protected function enOnTrip($driver_id,  $value, $lat = \null, $lng = \null)
    {
        // $this->genUpdate(TripTables::COMISSIONED, ['on_trip' => $value], ['driver_id' => $driver_id]);
    }
    final public function addDriversImage($user_id, string $image_uri)
    {
        $sql = $this->findDriver($user_id);
        $output = [0 => 'notfound'];

        $driver = $this->findDriver($user_id);

        if ($driver) {

            if ($this->genUpdate(TripTables::DRIVER, ['image_url' => $image_uri], ['id' => $driver['id']])) {
                $driver =  $this->findDriver($user_id);
                $output = [
                    0 => 'updated',
                    1 => $driver
                ];
            }
        }
        return $output;
    }
    /**
     * @param float {$lat}
     * @param float {$lng}
     * @param float {$distance}
     * 
     * @return array
     */
    public function findFirstDriver(float $lat = 0.09, float $lng = 0.18, float $distance = 30)
    {
        $sql = "
             SELECT a.id,a.name, a.driver_no, a.image_url, 
             c.name_plate, c.model, c.image_cover, c.id as `vehicle_id`,
             d.on_trip,
             ST_Latitude(d.position) as `lat`,  ST_Longitude(d.position) as `lng`,
             IFNULL(ST_Distance_Sphere( d.position, ST_GeomFromText( 'POINT({$lat} {$lng})', 4326, 'axis-order=lat-long' )), 0)  AS `distance`
        ";
        $sql .= " FROM  `" . TripTables::DRIVER . "` a ";
        $sql .= " INNER JOIN `" . TripTables::VEHICLES_DRIVER . "` b ON a.id = b.driver_id ";
        $sql .= " INNER JOIN `" . TripTables::VEHICLES . "` c ON b.vehicle_id = c.id ";
        $sql .= " INNER JOIN `" . TripTables::COMISSIONED . "` d ON b.driver_id = d.driver_id ";

        $sql .= " WHERE (a.status = 'verified' AND a.is_active='true' AND d.on_trip = 'false')";
        $sql .= " ORDER BY distance ASC ";
        $sql .= " LIMIT 1";
        //   return $sql;
        $this->query($sql);
        $driver = $this->single();

        if ($driver) {
            return $driver;
        }
        return null;
    }

    private function findDriver($user_id)
    {
        return $this->retdata('SELECT * FROM `' . TripTables::DRIVER . '` WHERE `user_id` = ' .(int) $user_id . ';');
    }

    final public function driverProfile(int $user_id)
    {
        $output = [0 => 'notfound'];
        $driver = $this->findDriver($user_id);
        if ($driver != null) {
            $output[0] = 'exists';
            $output[1] = $driver;
        }
        return $output;
    }
    
    final public function registerDriver(int $user_id, string $name, string $permit_class, bool $has_vehicle, $admin_id = null)
    {
        $output = [0 => 'error'];
        $driver = $this->findDriver($user_id);
        if (!$driver) {
            $dbDriver = [
                'driver_no' => \rand(0001, 9999),
                'user_id' => (int) $user_id,
                'name' => $name,
                'permit_class' => $permit_class,
                'has_vehicle' => $has_vehicle
            ];

            if ((int) $admin_id !== 0) {
                $dbDriver['admin_id'] = (int) $admin_id;
                $dbDriver['status'] = 'verified';
                $dbDriver['verified_at'] = $this->mytime();
            }
            if ($this->genInsert(TripTables::DRIVER, $dbDriver)) {
                $output[0] = 'inserted';
                $output[1] =  $this->findDriver($user_id);
            } else {
                $output[0] = 'error';
            }
        } else {
            $output[0] = 'exists';
            $output[1] = $driver;
        }

        return $output;
    }

    final public function driverEditProfile(int $user_id, string $name, string $permit_class, bool $has_vehicle)
    {
        $output = [0 => 'error'];
        $driver = $this->findDriver($user_id);
        if ($driver) {
            $dbDriver = [
                'driver_no' => \rand(0001, 9999),
                'user_id' => (int) $user_id,
                'name' => $name,
                'permit_class' => $permit_class,
                'has_vehicle' => $has_vehicle
            ];

            if ($this->genUpdate(TripTables::DRIVER, $dbDriver, ['id' => $driver['id']])) {
                $output[0] = 'inserted';
                $output[1] = $this->findDriver($user_id);
            } else {
                $output[0] = 'error';
            }
        } else {
            $output[0] = 'notfound';
        }

        return $output;
    }
    final public function driverDelete($user_id)
    {
        $output = [0 => 'notfound'];
        $driver = $this->findDriver($user_id);

        if ($driver) {
            if ($driver['status'] === 'pending') {
                $this->genDelete(TripTables::DRIVER, ['id' => $driver['id']]);
                $output = [
                    0 => 'deleted',
                ];
            } else {
                $this->genUpdate(TripTables::DRIVER, ['status' => 'deleted']);
                $output = [
                    0 => 'softdeleted',
                ];
            }
        }
        return $output;
    }
}