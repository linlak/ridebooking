<?php

namespace RidesBooking\Support\Traits;

trait HandlesRequest {
    final public function retrievePostData()
    {
        $data = [];
        if (isset($_POST)) {
            $json = file_get_contents('php://input');

            if (($data = @json_decode($json, true)) === null) {
                $data = $_POST;
            }
        }
        return $data;
    }
}