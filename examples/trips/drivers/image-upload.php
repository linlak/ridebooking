<?php

require_once('../../bootstrap.php');


$status = 'failed';
$message = 'Sorry fix a few things';
$output = [];

if (isset($_GET['id']) && $_GET['id']!=null)
{
    $imageDir =  '../../uploads';

    $imageData = $tripCtonroller->val_pic('image', 'image', $imageDir);

    if ($imageData!= null) {
        if (!file_exists($imageDir)) {
            mkdir($imageDir, 0777, true);
        }
        if(move_uploaded_file($_FILES['image']['tmp_name'], $imageDir.'/'.$imageData[1])) {
            $result = $tripCtonroller->addDriversImage($_GET['id'], $imageData[1]);

            switch($result[0]) {
                case 'updated':
                    $status = 'updated';
                    $driver = $result[1];

                    switch ($driver['status']) {
                        case 'pending':
                            $level = 1;
                            $message = 'You need to complete your registration';
                            break;
                        case 'verified':
                            $level = 2;
                            $message = 'All good';
                            break;
                        case 'deleted':
                            $level = 3;
                            $message = 'Your info was deleted';
                    }
                    if ($driver['status'] != 'deleted') {
                        $formartedDriver = [
                            'id' => (int) $driver['id'],
                            'name' => $driver['name'],
                            'permit_class' => $driver['permit_class'],
                            'has_vehicle' => (bool) $driver['has_vehicle'],
                            'status' => $driver['status']
                        ];
                        if ($driver['image_url']) {
                            $formartedDriver['image_url'] = $driver['image_url'];
                        }
                        $output['driver'] = $formartedDriver;
                    }
                    $output['level'] = $level;
                break;
                case 'notfound':
                    $status = 'notfound';
                    $message= 'You cant perform this action';
                    break;
            }
        }
    } else {
        if (count($tripCtonroller->getErrs()) > 0) {
            $message=$tripCtonroller->getErrs('image');
        }
    }
}

$output['status'] = $status;
$output['message'] = $message;

$tripCtonroller->echoJson($output);
