<?php

require_once('../../bootstrap.php');


    $status = 'failed';
    $message = 'Sorry fix a few things';
    $output = [];


    $data = $tripCtonroller->retrievePostData();

    if (isset($data) && isset($data['name']) && isset($data['permit_class'])) {
        $name = $data['name'];
        $permit_class = $data['permit_class'];
        $has_vehicle = (isset($data['has_vehicle'])) ? $data['has_vehicle'] : false;
        $result = $tripCtonroller->registerDriver((int) $_GET['id'], $name, $permit_class, $has_vehicle);
        switch ($result[0]) {
            case 'error':
                $message = 'Error saving data try again';
                break;
            case 'exists':
                $message = 'Driver already registered';
                $status = 'exists';
                $output['driver'] = $result[1]['id'];
                break;
            case 'inserted': 
                $status = 'success';
                $message = 'Data saved successfully';
            $driver = $result[1];

            switch ($driver['status']) {
                case 'pending':
                    $level = 1;
                    $message = 'You need to complete your registration';
                    break;
                case 'verified':
                    $level = 2;
                    $message = 'All good';
                    break;
                case 'deleted':
                    $level = 3;
                    $message = 'Your info was deleted';
            }
            if ($driver['status'] != 'deleted') {
                $formartedDriver = [
                    'id' => (int) $driver['id'],
                    'name' => $driver['name'],
                    'permit_class' => $driver['permit_class'],
                    'has_vehicle' => (bool) $driver['has_vehicle'],
                    'status' => $driver['status']
                ];
                if ($driver['image_url']) {
                    $formartedDriver['image_url'] = $driver['image_url'];
                }
                $output['driver'] = $formartedDriver;
            }
            $output['level'] = $level;
            break;
            default:
            $message = "Unknown error!";
                break;
        }        
    } else {
        $message = "Invalid data submitted";
    }

    $output['status'] = $status;
    $output['message'] = $message;

    $tripCtonroller->echoJson($output);