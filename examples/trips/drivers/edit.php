<?php

require_once('../../bootstrap.php');


$status = 'failed';
$message = 'Sorry fix a few things';
$output = [];

$data = $tripCtonroller->retrievePostData();

if (isset($data) && isset($data['name']) && isset($data['permit_class'])) {
    $name = $data['name'];
    $permit_class = $data['permit_class'];
    $has_vehicle = (isset($data['has_vehicle'])) ? $data['has_vehicle'] : false;
    $result = $tripCtonroller->driverEditProfile((int) $_GET['id'], $name, $permit_class, $has_vehicle);
    switch ($result[0]) {
        case 'notfound':
            $message = 'Not a driver register to become a driver';
            break;
        case 'exists':
            $level = 0;
            $status = true;
            $driver = $result[1];
            $message = 'Changes saved succefully';
            switch ($driver['status']) {
                case 'pending':
                    $level = 1;
                    $message = 'You need to complete your registration';
                    break;
                case 'verified':
                    $level = 2;
                    $message = 'All good';
                    break;
                case 'deleted':
                    $level = 3;
                    $message = 'Your info was deleted';
            }
            if ($driver['status'] != 'deleted') {
                $formartedDriver = [
                    'id' => (int) $driver['id'],
                    'name' => $driver['name'],
                    'permit_class' => $driver['permit_class'],
                    'has_vehicle' => (bool) $driver['has_vehicle'],
                    'status' => $driver['status']
                ];
                if ($driver['image_url']) {
                    $formartedDriver['image_url'] = $driver['image_url'];
                }
                $output['driver'] = $formartedDriver;
            }
            $output['level'] = $level;
            break;
    }
}
$output['status'] = $status;
$output['message'] = $message;

$tripCtonroller->echoJson($output);