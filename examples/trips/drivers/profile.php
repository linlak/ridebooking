<?php

require_once('../../bootstrap.php');


$status = false;
$message = 'Not a driver register to become a driver';
$output = [];
// authentication
if (isset($_GET['id']) && $_GET['id']!=null) {
    $result = $tripCtonroller->driverProfile((int) $_GET['id']);
    switch($result[0]) {
        case 'notfound':
            $message = 'Not a driver register to become a driver';
        break;
        case 'exists':
            $level = 0;
            $status = true;
            $driver = $result[1];

            switch($driver['status']) {
                case 'pending':
                    $level = 1;
                    $message = 'You need to complete your registration';
                break;
                case 'verified':
                    $level = 2;
                    $message = 'All good';
                break;
                case 'deleted':
                    $level = 3;
                    $message = 'Your info was deleted';
            }
            if ($driver['status']!='deleted') {
                $formartedDriver = [
                    'id' => (int)$driver['id'],
                    'name' => $driver['name'],
                    'permit_class' => $driver['permit_class'],
                    'has_vehicle' => (bool)$driver['has_vehicle'],
                    'status' => $driver['status']
                ];
                if ($driver['image_url']) {
                    $formartedDriver['image_url'] = $driver['image_url'];
                }
                $output['driver'] = $formartedDriver;
            }
            $output['level'] = $level;
        break;
    }
}
else {
    $message = 'Invalid data passed';
}
$output['status'] = $status;
$output['message'] = $message;

$tripCtonroller->echoJson($output);